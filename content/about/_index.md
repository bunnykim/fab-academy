---
title: "Hi, There!"
date: 2020-02-05
weight: 2
---

I'm Boeun(pronounced bo-un).
I study Master's program in New Media Design & Production at Aalto University. Here is where I showcase documentation for Fab Academy 2020!

Long story short, I'm a Korean-Canadian Artist/Designer living in Finland.
It's funny where life takes you eh?

My main study focus was in Fine Arts (Drawing/Painting) for as long as I can remember. Nonetheless, I've always been interested in technologies and New Media during my studies in Art that it often merged with my practice. My interests topic was diasporan identity in the age of digital era. While working as a Graphic Designer at an IT consulting company, I've become infatuated by technology and decided to study New Media at Aalto University.

I had a little experience in Digital Fabrication--digital prototyping (fusion 360), laser cutting, electronics, and coding--but the main reason that led me to Fab Academy was to hone my skills and knowledge to create practically make (almost) anything. I'm looking forward to my journey at Fab Academy 2020!
