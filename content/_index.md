---
title: "Hi, There!"
date: 2020-01-29
---

My name is Boeun _(pronounced bo-un)_.
I study Master's program in New Media Design & Production at Aalto University. Here is where I showcase documentation for Fab Academy 2020!

![Super wide](https://live.staticflickr.com/194/472097903_b781a0f4f8_z.jpg))
