---
title: 'Fab Academy 2020'
date: 2020-01-29
weight: 1
---

## Schedule

At the Fab Academy, you will learn how to envision, prototype and document your ideas through many hours of hands-on experience with digital fabrication tools. We take a variety of code formats and turn them into physical objects.

For more information check [Fab Academy](https://fabacademy.org/about/#what-you-ll-learn) website.





Jan 29: principles and practices (video), project management (video, review)
Feb 03 recitation: version control (video)
Feb 05: computer-aided design (video)
Feb 12: computer-controlled cutting
Feb 17 recitation:
Feb 19: electronics production
Feb 26: 3D scanning and printing
Mar 02 recitation:
Mar 04: electronics design
Mar 11: computer-controlled machining
Mar 16 recitation:
Mar 18: embedded programming
Mar 25: input devices
Mar 30 recitation:
Apr 01: applications and implications
Apr 08: break
Apr 15: output devices
Apr 20 recitation:
Apr 22: molding and casting
Apr 29: networking and communications
May 04 recitation:
May 06: interface and application programming
May 13: mechanical design, machine design
May 18 recitation:
May 20: wildcard week
May 27: invention, intellectual property, and income
Jun 01 recitation:
Jun 03: project development
Jun 10: project presentations
Jun 12: project presentations
Jun 15: project presentations
Jun 17: project presentations